# PYTHON VERSION: Python3

from random import *
import time
import operator

# FUNCTIONS
def displayTop():
    global old_data
    #display top 10
    #Code line retrieved from: https://stackoverflow.com/questions/4110665/sort-nested-dictionary-by-value-and-remainder-by-another-value-in-python
    top_ten = sorted(old_data, key=lambda key: (key.time, key.errors))
    top_count = 0
    print ('Rank --- Name --- Time --- Errors')
    for player in top_ten:
        top_count += 1
        if (top_count <= 10):
            print ('%s .)  ---  %s  ---   %s:%s  ---   %s') % (str(top_count),player.username, str(player.time/60),str(player.time%60),str(player.errors) )
        else:
            pass
def import_records():
    filename = 'inputs.txt'
    try:
        object_list = []
        with open(filename) as f:
            object_list = f.read().splitlines() 
        return object_list
        f.close()
    except:
        w = open(filename, 'w')
        w.close()
        return None

def main(current_user, correct_count = 0, failed = 0, error_count = 0):
    global old_data
    global response
    start = time.time()
    #Generate 10 arithmetic problems
    while(correct_count<10 and not(failed)):
        question_type = randrange(1,4)
        if (question_type == 1) :
            # Addition
            first = randrange(10,99)
            second = randrange(10,99)
            answer = first + second

            prompt = '\n(%s correct | %s wrong answers) %s + %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)
            
            if (user_answer == answer):
                correct_count += 1
                print('Correct!')
            else:
                error_count += 1        
        elif (question_type == 2) :
            # Subtraction
            first = randrange(10,99)
            second = randrange(10,first)
            answer = first - second

            prompt = '\n(%s correct | %s wrong answers) %s - %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)

            if (user_answer == answer):
                correct_count += 1
                print('Correct!')
            else:
                error_count += 1
        elif (question_type == 3) :
            # Multiplication
            first = randrange(10,99)
            second = randrange(2,9)
            answer = first * second

            prompt = '\n(%s correct | %s wrong answers) %s * %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)

            if (user_answer == answer):
                correct_count += 1
                print('Correct! \n')
            else:
                error_count += 1
        elif (question_type == 4) :
            # Division
            first = randrange(10,99)
            second = randrange(2,9)
            answer = first / second

            prompt = '\n(%s correct | %s wrong answers) %s / %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)

            if (user_answer == answer):
                correct_count += 1
                print('Correct! \n')
            else:
                error_count += 1
        
        if(error_count>=10):
            failed=True
    
    record = int(time.time() - start)

    current_user.time = record
    current_user.errors = error_count
    
    if(failed):
        print ("You have failed us, human. You shall be terminated.")
        response = input("\n Do you want to try again? (Y/N)").upper()
    else:
        #if a record of the username exists
        exists = False
        for user in old_data:
            if (current_user.username == user.username):
                exists = True
                user_index = old_data.index(user)
        if exists:
            if(old_data[user_index].time > current_user.time):
                old_data[user_index].time = current_user.time
                old_data[user_index].errors = current_user.errors

                print ('New highscore achieved! New record: ' + str(current_user.time))
            
            elif(old_data[user_index].time == current_user.time):
                if(old_data[user_index].errors >= current_user.errors):
                    old_data[user_index].time = current_user.time
                    old_data[user_index].errors = current_user.errors

                    print ('New highscore achieved! New record: ' + str(current_user.time))

            else:
                print ('You did not surpass your old record. Current time: %s seconds \n' % (str(record)))
        else:
            old_data.append(current_user)
            print ('\n Record saved, new player!')
        try:
            post_username = input("You are playing again. Enter a username: ")
            if (post_username != ''):
                current_user = Users(post_username)
                response = 'Y'
            else:
                displayTop()
                response = 'N'
        except:
            displayTop()
            response = 'N'

    
# Solution based from: https://stackoverflow.com/questions/30420621/python-creating-object-instances-in-a-loop-with-independent-handling

def save_data(data):
    w = open('inputs.txt', 'w')
    for user in data:
        output = '%s %s %s \n' % (user.username, str(user.time), str(user.errors))
        w.write(output)

# CLASSES

class Users(object):
    username = ""
    time = 0
    errors = 0

    def __init__(self, username, time = 0, errors = 0):
        self.username = username
        self.time = time
        self.errors = errors
    def __str__(self):
        return '%s' % (self.username)



''' START OF PROGRAM '''

#Program asks user for username
current_user = Users(input("Please enter a username: "))


#Program asks user to begin game
response = input("Do you want to begin the game? (Y/N) ")
response = response.upper()

text_input = import_records()
old_data = []
for item in text_input:
    line = item.split()
    old_data.append(Users(line[0],int(line[1]),int(line[2])))
if (old_data == []):
    print ('No old records found. You are the first! \n')
while(response == 'Y'):
    main(current_user)
    
print ('\n Program Ended.')
try: 
    print ('\n Saving data...')
    save_data(old_data)
    print ('Data saved!\n')
    print (old_data)
except Exception as e:
    print ('error saving data: ' + e)
