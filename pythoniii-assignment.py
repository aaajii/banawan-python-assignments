#PYTHON VERSION: Python3

from random import *
import time
#Program 'Database'
    #user_logs {username:highscore}
user_logs = {}

#Program asks user for username
username = input("Please enter a username: ")

#Program asks user to begin game
response = input("Do you want to begin the game? (Y/N) ")
response = response.upper()
#Program start

while(response == 'Y'):
    start = time.time()
    error_count = 0
    correct_count = 0
    failed = False
    #Generate 10 arithmetic problems
    while(correct_count<2 and not(failed)):
        question_type = randrange(1,4)
        if (question_type == 1) :
            # Addition
            first = randrange(10,99)
            second = randrange(10,99)
            answer = first + second

            prompt = '\n(%s correct | %s wrong answers) %s + %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)

            if (user_answer == answer):
                correct_count += 1
                print('Correct!')
            else:
                error_count += 1
        elif (question_type == 2) :
            # Subtraction
            first = randrange(10,99)
            second = randrange(10,first)
            answer = first - second

            prompt = '\n(%s correct | %s wrong answers) %s - %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)

            if (user_answer == answer):
                correct_count += 1
                print('Correct!')
            else:
                error_count += 1
        elif (question_type == 3) :
            # Multiplication
            first = randrange(10,99)
            second = randrange(2,9)
            answer = first * second

            prompt = '\n(%s correct | %s wrong answers) %s * %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)

            if (user_answer == answer):
                correct_count += 1
                print('Correct! \n')
            else:
                error_count += 1
        elif (question_type == 4) :
            # Division
            first = randrange(10,99)
            second = randrange(2,9)
            answer = first / second

            prompt = '\n(%s correct | %s wrong answers) %s / %s = ' % (str(correct_count),str(error_count),str(first),str(second))
            user_answer = input(prompt)

            if (user_answer == answer):
                correct_count += 1
                print('Correct! \n')
            else:
                error_count += 1
        if(error_count>=10):
            failed=True
    record = int(time.time() - start)

    if(failed):
        print ("You have failed us, human. You shall be terminated.")
        response = input("\n Do you want to try again? (Y/N)").upper()
    else:
        #if a record of the username exists
        if username in user_logs:
            if(user_logs[username]['time'] > record):
                user_logs[username] = {'time':record, 'errors':error_count}
                print ('New highscore achieved!')
            elif(user_logs[username]['time'] == record):
                if(user_logs[username]['errors'] <= error_count):
                    user_logs[username] = {'time':record, 'errors':error_count}
                    print ('New highscore achieved!')
            else:
                print ('You did not surpass your old record. Current time: %s \n' % (str(record)))
        else:
            user_logs[username] = {'time':record, 'errors':error_count}
            print ('\n Record saved, new player!')
    try:
        post_username = input("You are playing again. Enter a username: ")
        username = post_username
        response = 'Y'
    except:
        #display top 10
        #Code line retrieved from: https://stackoverflow.com/questions/4110665/sort-nested-dictionary-by-value-and-remainder-by-another-value-in-python
        top_ten = sorted(user_logs, key=lambda key: (user_logs[key]['time'], user_logs[key]['errors']))
        top_count = 0
        print ('Rank --- Name --- Time --- Errors')
        for player in top_ten:
            top_count += 1
            if (top_count <= 10):
                print ('%s .)  ---  %s  ---   %s:%s  ---   %s') % (str(top_count),player, str(user_logs[player]['time']/60),str(user_logs[player]['time']%60),str(user_logs[player]['errors']) )
            else:
                pass
        response = 'N'