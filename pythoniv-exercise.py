censored = ['stupid', 'moron', 'ugly']
filename = raw_input('Enter a filename to censor:')
r = open(filename, 'r')
w = open(filename + '.censored', 'w')
for line in r:
    for item in censored:
        line.replace(item, '*' * len(item))
        w.write(line + '\n')
r.close()
w.close()
