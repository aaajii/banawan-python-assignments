name1 = "Jimuel William Pacifico Banawan"
name2 = "Chynna Nicolette Sadaya Sevilleno"

#turn both inputs into lowercase to be able to search all of the available letters

name1 = name1.lower()
name2 = name2.lower()

#get array version to loop at both inputs

name1_arr = []
name2_arr = []

for letter in name1:
    name1_arr.append(letter)
for letter in name2:
    name2_arr.append(letter)

#remove spaces

name1_arr.remove(' ')
name2_arr.remove(' ')

#remove existing letters from each other

for remove_this_letter in name1_arr:
    try:
        name2_arr.remove(remove_this_letter)
    except:
        pass

for remove_this_letter in name2_arr:
    try:
        name1_arr.remove(remove_this_letter)
    except:
        pass

#count remaining letters

name2_arr.extend(name1_arr)
num_letters_left = len(name2_arr)

#flames values

flames = ['FRIENDS','LOVERS','ACQUAINTANCES','MARRIED','ENEMIES','SOUL MATES']

#output flames

print flames[num_letters_left%6]