raw_string = 'That that is, is. That that is not, is not. Is that it? It is.'
raw_string = raw_string.lower()

cleaned_letters = []

for letter in raw_string:
    if(letter.isalpha() or letter == ' '):
        cleaned_letters.append(letter)

cleaned_string = ''.join(cleaned_letters)

string_arr = cleaned_string.split()
string_arr_modified = cleaned_string.split()
extracted_words = []

dict_count = {}

for word in string_arr:
    try:
        string_arr_modified.remove(word)
        extracted_words.append(word)
    except:
        pass

for word in extracted_words:
    dict_count[word] = raw_string.count(word)

print dict_count